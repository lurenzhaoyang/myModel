package com.test.mymodel;

public class EmotionClasses {
    public static String[] EMOTION_CLASSES = new String[]{
            "anger",
            "disgust",
            "fear",
            "happy",
            "normal",
            "sad",
            "surprised"
    };
}
